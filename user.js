const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interests: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}

// 1 Find all users who are interested in video games.

const videogames=Object.entries(users).
    filter(items=>items[1]
        .interests.toString()
            .includes("Video Games"))
// console.log(videogames);

// Find all users staying in Germany.

const inGermany=Object.entries(users)
.filter(items=>items[1]
    .nationality==="Germany")
console.log(inGermany);

// Find all users with masters Degree.
const degree=Object.entries(users)
    .filter(items=>items[1]
        .qualification==="Masters")